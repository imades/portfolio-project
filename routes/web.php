<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/','HomeController@index')->name('home.index');



Route::get('/project{slug}','HomeController@show')->name('project.show');//give a name to Route


Route::get('/actualite{actuaiteslug}','HomeController@showActualite')->name('actualite.show');//display details of actualites

Route::get('/service{id}','HomeController@showService')->name('service.show');

Route::post('/contact-us', 'ContactController@saveContact');













