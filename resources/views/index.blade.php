
<!-- Navigation -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark">

    <div class="container">        

        <!-- Image Logo -->

        <!-- Text Logo - Use this if you don't have a graphic logo -->
         <a class="navbar-brand logo-text page-scroll" href="#header">Imad</a>

        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#services">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#projects">Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#actualite">Actualités </a>
                </li>
                
                
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Contact</a>
                </li>
            </ul>
            
        </div> <!-- end of navbar-collapse -->
    </div> <!-- end of container -->
</nav> <!-- end of navbar --> 
<!-- end of navigation --> 


@extends('layout.master')



<!-- Header -->
<header id="header" class="header">
    <div class="container">
        <div class="row">
            <h2 class="h2-heading">  </h2>
            <div class="col-lg-6">
                <div class="text-container">
                    <h1 class="h1-large">Hey, I am EL ISSAOUI Imad-Eddine</h1>
                    <h2 class="h2-lr">Back end developer</h2>
                    <a class="btn-solid-lg page-scroll" href="#about">Discover</a>
                    <a class="btn-outline-lg page-scroll" href="#contact"><i class="fas fa-user"></i>Contact Me</a>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    
    </div> <!-- end of container -->
</header> <!-- end of header -->
<!-- end of header -->

@section('content')
    


 <!-- About-->
 <div id="about" class="basic-1 bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h2 style="text-align: center">About</h2><br>
            </div>       
            <div class="col-lg-4">
                <div class="text-container first">
                    <h2>Hi there I'm Imad,</h2>
                    <p>And I love to create beautiful and efficient websites for my customers. I love going through the entire process with the customer from concept, to design and then development and launch</p>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
            <div class="col-lg-4">
                <div class="text-container second">
                    <div class="time">2021 - PRESENT</div>
                    <h6>Freelance Web Developer</h6>
                    <p>Working happily on my own web projects</p>
                    <div class="time">2019 - 2020</div>
                    <h6>Lead Web Developer</h6>
                    <p>Beautiful project for a major digital agency</p>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
            <div class="col-lg-4">
                <div class="text-container third">
                    <div class="time">2018 - 2019</div>
                    <h6>Senior Web Designer</h6>
                    <p>Inhouse web designer for ecommerce firm</p>
                    <div class="time">2017 - 2018</div>
                    <h6>Junior Web Designer</h6>
                    <p>Junior web designer for small web agency</p>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of basic-1 -->
<!-- end of about -->


<!-- Services -->
 <div id="services" class="basic-2">
    <div class="container">
        <div class="row">   
            <div class="col-lg-12">
                <h2 class="h2-heading">Services</h2><br>
                <p class="p-heading">Web design and development have been my bread and butter for more than 5 years. During that time I've discovered that I can help startups and companies with the following services</p>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
        <div class="row">

            @if (count($services)>0)
            @foreach($services as $serviceItem ) 
            <div class="col-lg-4">

                <div class="text-box">
                    <img class="img-fluid"  src="{{ asset('storage/'. $serviceItem->image)}}" alt="alternative">
                    <h4>{{$serviceItem->title}}</h4>
                    <a  href="{{route('service.show',$serviceItem->id)}}" >See More</a>

                </div> <!-- end of text-box -->
            </div> <!-- end of col -->
            @endforeach 
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div>
 <!-- end of basic-2 -->

@else
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-1 text-muted">
            <div class="h2-heading" >Nothing found </div>
            </div>
    
        </div>
    </div>
</div>
@endif    

<!-- end of services -->





<!-- Projects -->

 <div id="projects" class="basic-3">
    <div class="basic-4">
   <div class="container">     
       <div class="row">
           <div class="col-lg-12">
               <h2 class="h2-heading">Project</h2>
           </div>
           @if (count($project)>0)

           @foreach($project as $projectItem )      
           <div class="col-lg-4">
               <div class="text-container">
                   <div class="image-container">
                       <a href="{{route('project.show',$projectItem->slug)}}">
                           <img class="img-fluid"  src="{{ asset('storage/'. $projectItem->image)}}" alt="alternative">
                       </a>
                   </div> <!-- end of image-container -->
                   <h3 class="mb-0"> {{$projectItem->title}}</h3>
                   <div class="mb-1 text-muted">Creation Date : {{$projectItem->date_project}}</div>
                   <p>Project type : {{$projectItem->type}}</p>
                   <button type="button" class="btn btn-info"><a class="det" href="{{route('project.show',$projectItem->slug)}}" >Details</a></button>

               </div> <!-- end of text-container -->
           </div> <!-- end of col -->
           @endforeach 
       </div> <!-- end of row -->
      
    </div> <!-- end of container -->
</div>  
@else
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-1 text-muted">
            <div class="h2-heading" >Nothing found </div>
            </div>
    
        </div>
    </div>
</div>
@endif   

{{-- end --}}
<!-- end of basic-4 -->



<!-- Actualitéss -->

<div id="actualite" class="basic-4">
    <div class="container">   
<div class="row mb-2">
    <div class="col-lg-12">
        <h2 class="h2-heading">Actualités</h2>
    </div>
    @if (count($actualites)>0)

    @foreach($actualites as $actualitetItem ) 
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">{{$actualitetItem->subject}}</strong>
          <h3 class="mb-0">{{$actualitetItem->title}}</h3>
          <div class="mb-1 text-muted">{{$actualitetItem->type}} </div>
          <div class="mb-1 text-muted">{{$actualitetItem->created_at}}</div>
          
          <a href="{{route('actualite.show',$actualitetItem->actuaiteslug)}}" class="stretched-link">Continue reading</a>

        </div>
        
      </div>
    </div>
    @endforeach
    
  </div>
</div>
    </div>
</div>
@else
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-1 text-muted">
            <div class="h2-heading" >Nothing found </div>
            </div>
    
        </div>
    </div>
</div>
@endif   


<!-- Section Divider -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <hr class="section-divider">
        </div> <!-- end of col -->
    </div> <!-- end of row -->
</div> <!-- end of container -->
<!-- end of section divider -->




<!-- Contact -->
<div id="contact" class="form-1 bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Contact Us</h2>
                <p class="p-heading">For any type of online project please don't hesitate to get in touch with me. The fastest way is to send me your message using the following email <a class="blue no-line" href="mailto:imad.elissaouii@gmail.com">imad.elissaouii@gmail.com</a></p>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
        
                <!-- Contact Form -->
                <form method="post" action="contact-us" id="contactForm">
                    <div class="row">
                        <div class="col-lg-12">
                            @if(Session::has('success'))
                          <div class="alert alert-success">
                            {{ Session::get('success') }}
                           </div>
                       @endif
                    {{csrf_field()}}
                    <div class="form-group">
                        
                        <input type="text" class="form-control-input @error('name') is-invalid @enderror" id="name" name="name">
                        <label class="label-control" for="name">Name</label>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        
                        <input type="text" class="form-control-input @error('subject') is-invalid @enderror" id="subject" name="subject">
                        <label class="label-control" for="subject">Subject</label>

                        @error('subject')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control-input @error('email') is-invalid @enderror" id="cemail" name="email">
                        <label class="label-control" for="cemail">Email</label>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                      
                    </div>
                    <div class="form-group">
                        <textarea class="form-control-input textarea @error('message') is-invalid @enderror"  id="cmessage" name="message"></textarea>
                        @error('message')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="label-control" for="cmessage">Project details</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control-submit-button">Submit</button>
                    </div>
                </form>
                <!-- end of contact form -->

            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of form-1 -->  
<!-- end of contact -->



@endsection