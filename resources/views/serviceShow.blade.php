<!-- Navigation -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark">

    <div class="container">        

        <!-- Image Logo -->

        <!-- Text Logo - Use this if you don't have a graphic logo -->
         <a class="navbar-brand logo-text page-scroll" href="#header">Imad</a>

        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#home'>Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll"  href='{{route('home.index')}}/#about'>About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#services'>Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#projects'>Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#actualite'>Actualités </a>
                </li>
                
                
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#contact'>Contact</a>
                </li>
            </ul>
            
        </div> <!-- end of navbar-collapse -->
    </div> <!-- end of container -->
</nav> <!-- end of navbar --> 
<!-- end of navigation --> 


@extends('layout.master')

    @section('content')
        
   
<div id="main-content" class="blog-page">
    <div class="container">

<h2>Services Details</h2>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 left-box">
                <div class="card single_post">
                    <br><br><br>

                    <div class="body">
                        <div class="img-post">
                            <img class="d-block img-fluid"  src="{{ asset('storage/'. $service->image)}}" alt="First slide">
                        </div>

                        <h3>{{$service->title}}</h3>
                        {{$service->price}} <i class="fas fa-dollar-sign fa-1x ">    </i> 
                        <div class="mb-1 text-muted">Creation Date : {{$service->created_at}}</div>
                        <br>

                        <p>{!! $service->description !!}</p>
                                                {{--    '!!'  pour interpreter le Html  --}}
                                                <br><br>
                        <i class="fas fa-user-shield ">    {{Auth::user()->name}}</i> 

                        
                 
                    </div>                          
                                                
                </div>
                                   
     

            </div>
        </div>  
    </div>
</div>       

   @endsection





    
    







    
