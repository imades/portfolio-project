<!-- Navigation -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark">

    <div class="container">        

        <!-- Image Logo -->

        <!-- Text Logo - Use this if you don't have a graphic logo -->
         <a class="navbar-brand logo-text page-scroll" href="#header">Imad</a>

        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#home'>Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll"  href='{{route('home.index')}}/#about'>About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#services'>Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#projects'>Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#actualite'>Actualités </a>
                </li>
                
                
                <li class="nav-item">
                    <a class="nav-link page-scroll" href='{{route('home.index')}}/#contact'>Contact</a>
                </li>
            </ul>
            
        </div> <!-- end of navbar-collapse -->
    </div> <!-- end of container -->
</nav> <!-- end of navbar --> 
<!-- end of navigation --> 



@extends('layout.master')

    @section('content')
        
   
<div id="main-content" class="blog-page">
    <div class="container">

        <h2>Projects Details</h2>

        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 left-box">
                <div class="card single_post">
                    <div class="body">
                        <div class="img-post">
                            <img class="d-block img-fluid"  src="{{ asset('storage/'. $project->image)}}" alt="First slide">
                        </div>
                        <h3>{{$project->title}}</h3>
                        <div class="mb-1 text-muted">Creation Date : {{$project->date_project}}</div>
                        <p>{!! $project->description !!}</p>
                                                {{--    '!!'  pour interpreter le Html  --}}
                            @if (Auth::check()) {
                                <i class="fas fa-user-shield ">   
                                    {{Auth::user()->name}}</i> 

                            }
                            @endif


                        
                 
                 </div>                          
                    </div>                        
                </div>
                                   
     

            </div>
        </div>  
    </div>
</div>       
   @endsection





    
    







    
