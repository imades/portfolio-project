<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;use Mail; 

class ContactController extends Controller
{
    public function saveContact(Request $request) { 

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'subject' => 'required',
        ]);

        $contact = new Contact;$mail = new Mail;


        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->subject =$request->subject;
        $contact->save();
        $mail::send('layout.master',
        
             array(
                 'name' => $request->get('name'),
                 'email' => $request->get('email'),
                 'subject' => $request->get('subject'),
                 'user_message' => $request->get('message'),
             ), function($message) use ($request)
               {
                  $message->from($request->email);
                  $message->to('imad.elissaouii@gmail.com');
                  
               });
        
        return back()->with('success', 'Thank you for contact us!');

    }

}
