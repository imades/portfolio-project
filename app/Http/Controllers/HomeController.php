<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Actualite;
use App\Services;

class HomeController extends Controller
{
    public function index(){


        $project= Project::all();
        $actualites=Actualite::all();
        $services=Services::all();

       
    return view('index')->with('project', $project)->with('actualites',$actualites)->with('services',$services);

    }

// display project details
    public function show($slug){
        $project= Project::where('slug',$slug)->firstOrfail();
        return view('ProjectShow')->with('project',$project);

    }


// display actualite details
    public function showActualite($actuaiteslug){

        $actualite= Actualite::where('actuaiteslug',$actuaiteslug)->firstOrfail();
        return view('ActualitetShow')->with('actualite',$actualite);

    }
// display service details
public function showService($id){

    $service= Services::where('id',$id)->firstOrfail();
    return view('serviceShow')->with('service',$service);

}


}
